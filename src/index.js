var jsdom = require('jsdom');
const { JSDOM } = jsdom;

var formatDate = function (date){
    var year = date.getFullYear();
    if(date.getMonth() < 9)
        var month = '0' + (date.getMonth() + 1);
    else
        var month = date.getMonth() + 1;
    if(date.getDate() < 10)
        var day = '0' + date.getDate();
    else
        var day = date.getDate();
    return month + '/' + day + '/' + year;
};

self.onmessage = function (message) {
   const { window } = new JSDOM(message.data);
	var $ = require('jquery')(window); 
	
	var data = {};
	data.data_type = 'scores'; 
	data.experian_score = $('#credit-scores .scoreVal')[0].innerHTML;
	data.equifax_score = $('#credit-scores .scoreVal')[1].innerHTML;
	data.transunion_score = $('#credit-scores .scoreVal')[2].innerHTML;
	self.postMessage(data);

	accounts_iter = $('h3:contains(Accounts)');
	accounts_iter = accounts_iter.next();
	while(accounts_iter.prop('tagName') == 'TABLE'){
		data = {};
		data.data_type = 'account';
        if(accounts_iter.find('tr').eq(0).find('th').first().hasClass('warning')){
        	data.item_status_id = 1;
        }else{
        	data.item_status_id = 3;
        }

        account_name = accounts_iter.find('tr').eq(0).find('th').first().text().trim();
        data.account_name = account_name;

        if(accounts_iter.find('tr').eq(1).find('td').eq(0).text().trim() != ''){
        	data.experian = {};
            data.experian.derogatories = {
                '30' : [],
                '60' : [],
                '90' : [],
                '120' : [],
                '150' : [],
                '180' : [],
                'PP' : [],
                'RF' : [],
                'CO' : []
            };
        }
        if(accounts_iter.find('tr').eq(1).find('td').eq(1).text().trim() != ''){
            data.equifax = {};
            data.equifax.derogatories = {
                '30' : [],
                '60' : [],
                '90' : [],
                '120' : [],
                '150' : [],
                '180' : [],
                'PP' : [],
                'RF' : [],
                'CO' : []
            };
        }
        if(accounts_iter.find('tr').eq(1).find('td').eq(2).text().trim() != ''){
            data.transunion = {};
            data.transunion.derogatories = {
                '30' : [],
                '60' : [],
                '90' : [],
                '120' : [],
                '150' : [],
                '180' : [],
                'PP' : [],
                'RF' : [],
                'CO' : []
            };
        }

        for(var i = 0; i<=2; i++){
            var bureau;
            if(i==0)
                bureau = 'experian';
            if(i==1)
                bureau = 'equifax';
            if(i==2)
                bureau = 'transunion';
            //account number
            tr = accounts_iter.find('th:contains(Account Number)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
            	data[bureau].account_number = tr.find('td').eq(i).text().trim();

            //account status
            tr = accounts_iter.find('th:contains(Account Status)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].account_status = tr.find('td').eq(i).text().trim();
            //account type
            tr = accounts_iter.find('th:contains(Account Type)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].account_type = tr.find('td').eq(i).text().trim();
            //account balance
            tr = accounts_iter.find('th:contains(Account Balance)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].account_balance = tr.find('td').eq(i).text().trim();
            //monthly payment
            tr = accounts_iter.find('th:contains(Monthly Payment)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].monthly_payment = tr.find('td').eq(i).text().trim();
            //high balance
            tr = accounts_iter.find('th:contains(High Balance)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].high_balance = tr.find('td').eq(i).text().trim();
            //classification
            tr = accounts_iter.find('th:contains(Classification)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].classification = tr.find('td').eq(i).text().trim();
            //limit
            tr = accounts_iter.find('th:contains(Limit)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].account_limit = tr.find('td').eq(i).text().trim();
            //date opened
            tr = accounts_iter.find('th:contains(Date Opened)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != ''){
                var date = new Date(tr.find('td').eq(i).text().trim());
                if(date.toDateString() != 'Invalid Date')
                    data[bureau].date_opened = formatDate(date);
            }
            //responsibility
            tr = accounts_iter.find('th:contains(Responsibility)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].responsibility = tr.find('td').eq(i).text().trim();
        }
        derogatories = accounts_iter.find('tr').eq(25);
        derogatories.find('tr.crhdata').each(function(i,e){
            var year = $(e).find('th').text().trim();
            $(e).find('> td').each(function(i1,e1){
                var month = i1 + 1;
                if(month < 10)
                    month = '0' + (i1 + 1);
                $(e1).find('table.data td').each(function(i2,e2){
                    if(i2 == 0){
                        var bureau = 'experian';
                    }
                    if(i2 == 1){
                        var bureau = 'equifax';
                    }
                    if(i2 == 2){
                        var bureau = 'transunion';
                    }
                    if($(e2).text().trim() == '30'
                        || $(e2).text().trim() == '60'
                        || $(e2).text().trim() == '90'
                        || $(e2).text().trim() == '120'
                        || $(e2).text().trim() == '150'
                        || $(e2).text().trim() == '180'
                        || $(e2).text().trim() == 'PP'
                        || $(e2).text().trim() == 'RF'
                        || $(e2).text().trim() == 'CO'
                        ){
                    	data[bureau].derogatories[$(e2).text().trim()].push(month+'/'+year);
                    }
                });
            });
        });
        accounts_iter = accounts_iter.next();
        self.postMessage(data);
    }

    records_iter = $('h3:contains(Public Records)');
    records_iter = records_iter.next();
    while(records_iter.prop('tagName') == 'TABLE'){
    	data = {};
    	data.data_type = 'public_record';

        public_record_name = records_iter.find('tr').eq(0).find('th').first().text().trim();
        data.public_record_name = public_record_name;

        if(records_iter.find('tr').eq(1).find('td').eq(0).text().trim() != ''){
        	data.experian = {};
        }
        if(records_iter.find('tr').eq(1).find('td').eq(1).text().trim() != ''){
            data.equifax = {};
        }
        if(records_iter.find('tr').eq(1).find('td').eq(2).text().trim() != ''){
            data.transunion = {};
        }

        for(var i = 0; i<=2; i++){
            var bureau;
            if(i==0)
                bureau = 'experian';
            if(i==1)
                bureau = 'equifax';
            if(i==2)
                bureau = 'transunion';
            //court
            tr = records_iter.find('th:contains(Court)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].court = tr.find('td').eq(i).text().trim();

            //plaintiff
            tr = records_iter.find('th:contains(Plaintiff)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].plaintiff = tr.find('td').eq(i).text().trim();
            //type
            tr = records_iter.find('th:contains(Type)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != ''){
                type = '';
                if(tr.find('td').eq(i).text().trim().toLowerCase().indexOf('bankruptcy') >= 0)
                    type = 1;
                if(tr.find('td').eq(i).text().trim().toLowerCase().indexOf('tax') >= 0)
                    type = 2;
                data[bureau].type = type;
            }
            //reference
            tr = records_iter.find('th:contains(Reference)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].reference = tr.find('td').eq(i).text().trim();;
            //amount
            tr = records_iter.find('th:contains(Amount)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].amount = tr.find('td').eq(i).text().trim();
            //liability
            tr = records_iter.find('th:contains(Liability)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].liability = tr.find('td').eq(i).text().trim();
            //status
            tr = records_iter.find('th:contains(Status)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].status = tr.find('td').eq(i).text().trim();;
            //date completed
            tr = records_iter.find('th:contains(Date Completed)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != ''){
                var date = new Date(tr.find('td').eq(i).text().trim());
                if(date.toDateString() != 'Invalid Date')
                    data[bureau].date_completed = formatDate(date);
            }
        }
        records_iter = records_iter.next();
        self.postMessage(data);
    }

    inquiries_iter = $('h3:contains(Inquiry)');
    inquiries_iter = inquiries_iter.next();
    while(inquiries_iter.prop('tagName') == 'TABLE'){
        data = {};
        data.data_type = 'inquiry';

        inquiry_name = inquiries_iter.find('tr').eq(0).find('th').first().text().trim();
        data.inquiry_name = inquiry_name;

        if(inquiries_iter.find('tr').eq(1).find('td').eq(0).text().trim() != ''){
        	data.experian = {};
        }
        if(inquiries_iter.find('tr').eq(1).find('td').eq(1).text().trim() != ''){
            data.equifax = {};
        }
        if(inquiries_iter.find('tr').eq(1).find('td').eq(2).text().trim() != ''){
            data.transunion = {};
        }

        for(var i = 0; i<=2; i++){
            var bureau;
            if(i==0)
                bureau = 'experian';
            if(i==1)
                bureau = 'equifax';
            if(i==2)
                bureau = 'transunion';
            //inquirer
            tr = inquiries_iter.find('th:contains(Inquirer)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].inquirer = tr.find('td').eq(i).text().trim();
            //inquiry date
            tr = inquiries_iter.find('th:contains(Inquiry Date)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != ''){
                var date = new Date(tr.find('td').eq(i).text().trim());
                if(date.toDateString() != 'Invalid Date')
                	data[bureau].inquiry_date = formatDate(date);
            }
            //inquiry type
            tr = inquiries_iter.find('th:contains(Inquiry Type)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].inquiry_type = tr.find('td').eq(i).text().trim();
            //phone
            tr = inquiries_iter.find('th:contains(Phone)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].phone = tr.find('td').eq(i).text().trim();
            //address
            tr = inquiries_iter.find('th:contains(Address)').parents('tr');
            if(tr.find('td').eq(i).text().trim() != '')
                data[bureau].address = tr.find('td').eq(i).text().trim();
        }
        inquiries_iter = inquiries_iter.next();
        self.postMessage(data);
    }

	self.postMessage({data_type: 'done'});
}
