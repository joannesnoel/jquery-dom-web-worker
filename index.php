<!doctype html>
<html>
  <head>
    <title>Getting Started</title>
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.5/dist/loadingoverlay.min.js"></script>
  </head>
  <body>
    <script>
    	function importReport(html){
	        if(typeof(Worker) !== "undefined") {
		        if(typeof(w) == "undefined") {
		            w = new Worker("dist/main.js");
		        }
		        w.onmessage = function(event) {
		            console.log(event.data);
		            if(event.data == 'done'){
		            	w.terminate();
	    				w = undefined;
	    				$('body').LoadingOverlay("hide")
		            }
		        };
		        w.postMessage(html);
		    } else {
		        document.getElementById("result").innerHTML = "Sorry! No Web Worker support.";
		    }
		}

		var handleFileSelect = function(evt) {
            var html;
            var files = evt.target.files; // FileList object

            var file = files[0];
            var start = 0;
            var stop = file.size - 1;

            var reader = new FileReader();

            // If we use onloadend, we need to check the readyState.
            reader.onloadend = function(evt) {
              if (evt.target.readyState == FileReader.DONE) { // DONE == 2
                html = evt.target.result;
                $('body').LoadingOverlay("show")
                importReport(html);
              }
            };

            var blob = file.slice(start, stop + 1);
            reader.readAsBinaryString(blob);
        };
    </script>
    <input type="file" name="upload_file" value="Upload Pro Credit"> 
    <script type="text/javascript">
    	$('[name="upload_file"]').unbind().change(handleFileSelect);
    </script>
  </body>
</html>