const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  node: {
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      dns: 'empty',
      child_process: 'empty'
  }
};